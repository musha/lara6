<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// api 资源控制器
Route::resource('demo', 'Demo\UserController');
Route::resource('bio/user', 'Bio\UserController');
Route::resource('bio/info', 'Bio\InfoController');
