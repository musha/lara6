<?php

namespace App\Models\Bio;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    // db
    protected $table = 'bios';
    // 主键
    protected $primaryKey = 'uid';
    public $incrementing = false;
    // 赋值许可
    protected $fillable = ['uid', 'name', 'dcode', 'dname', 'timestamp', 'key'];
}
