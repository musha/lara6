<?php

namespace App\Models\Bio;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    // db
    protected $table = 'codes';
    // 主键
    protected $primaryKey = 'code';
    public $incrementing = false;
}
