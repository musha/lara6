<?php

namespace App\Models\Demo;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    //
    protected $table = 'users';
    // 全部许可
    protected $guarded = [];
}
