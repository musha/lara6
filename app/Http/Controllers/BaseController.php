<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
// use App\Models\Bio\Code;
class BaseController extends Controller
{	
    // 返回数据格式
    protected function formatData($code = 200, $msg = '', $data = []) : Response 
    {
    	$arr = ['code' => $code, 'msg' => $msg, 'data' => $data];
    	return response($arr);
    }
    // 生成随机码
    protected function randomCode() {
    	$code = '';
        for($i = 0; $i < 4; $i++) {
            $code .= chr(rand(65,90));
        };
        return $code;
    }
    // 取出随机码
    protected function getCode($uid) {
    	$item = DB::table('codes')->where('status', 0)->limit(1);
        $code = $item->value('code');
        $item->update([
            'uid' => $uid,
            'status' => 1
        ]);
        return $code;
    }
}
