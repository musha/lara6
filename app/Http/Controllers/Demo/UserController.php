<?php

namespace App\Http\Controllers\Demo;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Models\Demo\Demo;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 模型
        $user = Demo::get();
        // 是否已存在
        if($user -> isEmpty()) {
            // 为空
            return $this->formatData(404, '暂无数据');
        } else {
            return $this->formatData(200, '获取成功', Demo::select('name', 'phone')->get());
        };
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 模型
        $user = Demo::get();
        // // 得到传过来的值
        $name = $request -> post('name');
        $phone = $request -> post('phone');
        // // 数据库查找
        $newuser = Demo::where([
            ['name', '=', $name]
        ]) -> limit(1) -> get();
        if($newuser -> isEmpty()) {
            // 不存在 可加入
            $addData = Demo::create(\Request::all());
            if($addData) {
                return $this->formatData(200, '数据添加成功', $addData);
            };
        } else {
            return $this->formatData(204, '当前用户已存在');
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
