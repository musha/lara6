<?php

namespace App\Http\Controllers\bio;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Models\Bio\User;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return $this->formatData(200, '获取成功', User::get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 获取提交的数据
        $data = $request->all();
        // 验证
        $validator = Validator::make($data, [
            'uid' => 'required',
            'name' => 'required',
            'dcode' => 'required',
            'dname' => 'required',
            'timestamp' => 'required',
            'key' => 'required'
        ]);
        if($validator->fails()) {
            // 缺少参数
            return $this->formatData(204, '缺少参数');
        } else {
            // 验证加密
            $uid = $data['uid'];
            $timestamp = $data['timestamp'];
            $key = $data['key'];
            $KEY = '931754f678399cf1489d51307cd39f63';
            if($key == md5($uid.$timestamp.$KEY)) {
                $user = User::find($uid);
                // 对上了 检查是否存在
                if(isset($user->uid)) {
                    // 用户已存在
                    $user = User::where('uid', $uid)->get(['uid', 'name', 'dcode', 'dname', 'timestamp', 'key', 'hasanswer', 'score', 'code1', 'code2', 'code3', 'code1status', 'code2status', 'code3status']);
                    return $this->formatData(200, '当前用户已存在', $user);
                } else {
                    // 不存在 可加入
                    $addData = User::create(\Request::all());
                    if($addData) {
                        // 添加成功
                        $newuser = User::where('uid', $uid)->get(['uid', 'name', 'dcode', 'dname', 'timestamp', 'key', 'hasanswer', 'score', 'code1', 'code2', 'code3', 'code1status', 'code2status', 'code3status']);
                        return $this->formatData(200, '数据添加成功', $newuser);
                    };
                };
            } else {
                // 没有通过验证
                return $this->formatData(204, '非法账号'); 
            };
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
