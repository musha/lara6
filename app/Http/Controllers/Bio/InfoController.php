<?php

namespace App\Http\Controllers\Bio;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Bio\User;
use App\Models\Bio\Code;
use Illuminate\Support\Facades\Validator;

class InfoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // 产生四位随机码
        // $n = 0;
        // while($n < 1) {
        //         $n = $n + 1;
        //     DB::table('codes') -> insertOrIgnore([
        //      'code' => $this->randomCode()
        //     ]);
        // };
        // return DB::table('codes')->count();
        //return $this->getCode('0014');
        // return md5('0007'.'000000000000'.'931754f678399cf1489d51307cd39f63');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 获取提交的数据
        $data = $request->all();
        // 验证
        $validator = Validator::make($data, [
            'uid' => 'required',
            'timestamp' => 'required',
            'key' => 'required',
            'score' => 'required'
        ]);
        if($validator->fails()) {
            // 缺少参数
            return $this->formatData(204, '缺少参数');
        } else {
            // 验证加密
            $uid = $data['uid'];
            $timestamp = $data['timestamp'];
            $key = $data['key'];
            $KEY = '931754f678399cf1489d51307cd39f63';
            if($key == md5($uid.$timestamp.$KEY)) {
                $user = User::find($uid);
                // 对上了 检查是否存在
                if(isset($user->uid)) {
                    $oldscore = intval($user['score']);
                    $score = intval($data['score']);
                    // 只有大于当前分数才可存储
                    if($score > $oldscore) {
                        $user->score = $score;
                    };
                    // 判断分数是否可以给兑奖码
                    if($score >= 90) {
                        // 可以给兑奖码
                        $code1 = $user['code1'];
                        // 判断code1
                        if(empty($code1)) {
                            // code1为null
                            $code = $this->getCode($uid);
                            $user->code1 = $code;
                        } else {
                            // code1不为null 判断code2
                            $code2 = $user['code2'];
                            if(empty($code2)) {
                                // code2为null
                                $code = $this->getCode($uid);
                                $user->code2 = $code;
                            } else {
                                // code2不为null 判断code3
                                $code3 = $user['code3'];
                                if(empty($code3)) {
                                    // code3为null
                                    $code = $this->getCode($uid);
                                    $user->code3 = $code;
                                };
                            };
                        };
                    };
                    $user->hasanswer = 1;
                    $user->save();
                    $newuser = User::where('uid', $uid)->get(['uid', 'name', 'dcode', 'dname', 'timestamp', 'key', 'hasanswer', 'score', 'code1', 'code2', 'code3', 'code1status', 'code2status', 'code3status']);
                    return $this->formatData(200, '提交成功', $newuser);
                } else {
                    // 不存在
                    return $this->formatData(204, '用户不存在');
                };
            } else {
                // 没有通过验证
                return $this->formatData(204, '非法账号'); 
            };
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
